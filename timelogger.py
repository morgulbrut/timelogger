import sys
import datetime
from PyQt5 import QtCore, QtWidgets, uic


qtCreatorFile = "timelogger.ui"

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


class MyApp(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self):
        self.date = None
        self.date_string = ''
        self.logname = 'time.log'
        self.timeformat = '%d-%m-%y %H:%M:%S'  # or '%d-%m-%y %H:%M
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.start_stop_button.clicked.connect(self.log_time)
        self.start_stop_button.setStyleSheet("font: bold")
        self.is_logging = 'off'
        self.read_log(self.logname)

    def read_log(self, filename):
        try:
            self.conslog('INFO','Reading from {}'.format(filename))
            with open(filename, 'r') as f:
                lines = f.read().splitlines()
                try:
                    last_line = lines[-1]
                    try:
                        self.projectname_box.setText(last_line.split(',')[1])
                    except:
                        pass
                    if last_line.find('off') != -1:
                        self.set_logging(False)
                    elif last_line.find('on') != -1:
                        self.set_logging(True)
                except IndexError:
                    self.conslog('ERROR','IndexError in read_log')
                    self.set_logging(False)
        except FileNotFoundError:
            self.conslog('INFO','Setting up new file {}'.format(filename))
            with open(filename, 'w') as f:
                try:
                    f.write('Date/Time,Project,Logging,Time\n')
                    self.set_logging(False)
                except:
                    pass

    def conslog(self,level,msg):
        print('[{:8}]: {}'.format(level,msg))

    def set_logging(self, bool):
        if bool:
            self.is_logging = 'on'
            self.start_stop_button.setStyleSheet("color: green")
            self.start_stop_button.setText('Logging')
            self.projectname_box.setDisabled(True)
        else:
            self.is_logging = 'off'
            self.start_stop_button.setStyleSheet("color: red")
            self.start_stop_button.setText('Not logging')
            self.projectname_box.setDisabled(False)
        self.conslog('INFO','Set to {}'.format(self.is_logging))

    def log_time(self):
        diff = '-'
        now = datetime.datetime.now().strftime(self.timeformat)
        self.conslog('INFO','Logging at {}'.format(now))
        if self.is_logging == 'on':
            diff = self.calc_diff(now)
            self.set_logging(False)
        else:
            self.set_logging(True)
        with open(self.logname, 'a') as f:
            f.write('{},{},{},{}\n'.format(
                now, self.projectname_box.text(), self.is_logging, diff))

    def calc_diff(self,now):
        try:
            with open('time.log', 'r') as f:
                lines = f.read().splitlines()
                last_time = lines[-1:]
                try:
                    begin = self.parse_timestring(last_time[0].split(',')[0])
                    end = self.parse_timestring(now)
                    return end - begin
                except IndexError:
                    self.conslog('ERROR','IndexError in calc_diff')
        except FileNotFoundError:
            self.conslog('ERROR','FileNotFoundError')

    def parse_timestring(self, time_string):
        date = time_string.split(' ')[0]
        time = time_string.split(' ')[1]
        day = int(date.split('-')[0])
        month = int(date.split('-')[1])
        year = int(date.split('-')[2])
        hour = int(time.split(':')[0])
        minute = int(time.split(':')[1])
        try:
            second = int(time.split(':')[2])
        except:
            second = 0

        return datetime.datetime(year, month, day, hour, minute, second)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())
