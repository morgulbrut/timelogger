# Timelogger 

Simple time logger, stores your project times in an easy readable csv file

## Features:

 * Project description locked while logging
 * Gets the last state from the log file, so it can be shut down between logging

## Installing:

 * No dependencies beside PyQt4

## Cheers:

Shantnu for this: https://github.com/shantnu/PyQt_first